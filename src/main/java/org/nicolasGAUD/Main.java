package org.nicolasGAUD;


import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);  // Create a Scanner object
        System.out.println("Entrez un mot ou une phrase : ");
        String STRING_TO_CHECK = sc.nextLine().toLowerCase();  // Read user input
        Functions functions = new Functions();
        ArrayList<String> result = functions.countEachVowel(STRING_TO_CHECK);
        for (String str : result){
            System.out.println(str);
        }
        System.out.println("\nEt il y'a en tout " + functions.countAllVowels(STRING_TO_CHECK) + " voyelles dans la phrase.");
    }
}