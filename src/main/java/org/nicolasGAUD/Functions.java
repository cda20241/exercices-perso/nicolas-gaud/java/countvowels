package org.nicolasGAUD;

import java.text.Normalizer;
import java.util.ArrayList;

public class Functions {

    String [] VOWELS_LIST  = {"a", "e", "i", "o", "u", "y"};

    /*
    :param sentence: a sentence entered by a user
    :return: the number of vowels in the sentence
    */
    public int countAllVowels(String str){

        int countAllVowels = 0;

        String asciiSentence = strToAscii(str);

        for (int i = 0 ; i < str.length() ; i++) {
            for (String vowel : VOWELS_LIST) {
                if (asciiSentence.substring(i, i + 1).equals(vowel)) {
                    countAllVowels++;
                }
            }
        }
        return countAllVowels;
    }

    /*
    :param sentence: a sentence entered by a user
    :return: an arraylist of string messages containing yhe number of each vowel in the sentence
    */
    public ArrayList<String> countEachVowel(String str) {
        int countEachVowel = 0;
        ArrayList<String> result = new ArrayList<>();

        String asciiSentence = strToAscii(str);

        for (String vowel : VOWELS_LIST) {
            countEachVowel = 0;
            for (int i = 0; i < str.length(); i++) {
                if (String.valueOf(asciiSentence.charAt(i)).equals(vowel)) {
                    countEachVowel++;
                }
            }
            result.add("Il y'a " + countEachVowel + " \"" + vowel + "\" dans la phrase.");
        }
        return result;
    }

    /*
    :param sentence: a sentence entered by a user
    :this function normalizes a sentence all accents on letters (ie : "âäêëîï" ==> "aaeeii")
    :return: an arraylist of string messages containing the number of each vowel in the sentence
    */
    public String strToAscii(String str){
        // Normalisation NFKD
        String normalizedSentence = Normalizer.normalize(str, Normalizer.Form.NFKD);

        // Suppression des caractères non ASCII
        String asciiSentence = normalizedSentence.replaceAll("[^\\p{ASCII}]", "");

        return asciiSentence;
    }

}
