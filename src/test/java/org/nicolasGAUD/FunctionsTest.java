package org.nicolasGAUD;

import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;

class FunctionsTest {

    Functions functions = new Functions();
    @org.junit.jupiter.api.Test
    void countAllVowelsTest() {
        try{
            Assertions.assertEquals(18, functions.countAllVowels("engage le jeu que je le gagne âäêëîï"),
                    "\"engage le jeu que je le gagne âäêëîï\" contient 18 voyelles");
            System.out.println("Test 1 : countAllVowels : OK");
        }
        catch (AssertionError ex){
            System.out.println(ex);
        }
    }

    @org.junit.jupiter.api.Test
    void countEachVowelTest() {
        ArrayList<String> result = functions.countEachVowel("engage le jeu que je le gagne âäêëîï");
        try{
            Assertions.assertEquals("Il y'a 4 \"a\" dans la phrase.", result.get(0),
                    "Il y'a 4 \"a\" dans la phrase.");
            Assertions.assertEquals("Il y'a 10 \"e\" dans la phrase.", result.get(1),
                    "Il y'a 10 \"e\" dans la phrase.");
            Assertions.assertEquals("Il y'a 2 \"i\" dans la phrase.", result.get(2),
                    "Il y'a 2 \"i\" dans la phrase.");
            Assertions.assertEquals("Il y'a 0 \"o\" dans la phrase.", result.get(3),
                    "Il y'a 0 \"o\" dans la phrase.");
            Assertions.assertEquals("Il y'a 2 \"u\" dans la phrase.", result.get(4),
                    "Il y'a 2 \"u\" dans la phrase.");
            Assertions.assertEquals("Il y'a 0 \"y\" dans la phrase.", result.get(5),
                    "Il y'a 0 \"y\" dans la phrase.");
            System.out.println("Test 2 : countEachVowel : OK");
        }
        catch (AssertionError ex){
            System.out.println(ex);
        }
    }

    @org.junit.jupiter.api.Test
    void strToAsciiTest() {
        try{
            Assertions.assertEquals("engage le jeu que je le gagne aaeeii", functions.strToAscii("engage le jeu que je le gagne âäêëîï"),
                    "");
            System.out.println("Test 3 : strToAscii : OK");
        }
        catch (AssertionError ex){
            System.out.println(ex);
        }
    }
}